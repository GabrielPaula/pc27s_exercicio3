/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Gabriel Souza de Paula
 * Ultima modificacao: 01/10/2017
 */
package exemplo3;

import java.util.*;

public class VetorCompartilhado {

    private final int[] array;
    private int writeIndex=0; //indice do proximo elemento a ser gravado
    private static Random generator = new Random();

    public VetorCompartilhado(int size){
        array = new int[size];
    }//fim do construtor
    
    public synchronized void add(int value){
        
        int position = writeIndex;
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        
        //Insere um valor na posicao do vetor
        array[position] = value;
        System.out.printf("\n%s escreveu %2d na posicao %d", Thread.currentThread().getName(), value,position);
        
        //Incrementa o indice
        writeIndex++;
        System.out.printf("\nProximo indice: %d", writeIndex);
    }

    public synchronized void sub(){
        writeIndex--;

        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(500));
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        array[writeIndex] = 0;
                System.out.printf("\nAlterou o valor da  %s na posicao %d para zero.", Thread.currentThread().getName(),writeIndex);

        System.out.printf("\nProximo indice: %d", writeIndex);
    }
    
    //Metodo sobrecarregado
    public String toString(){
        return "\nVetor:\n" + Arrays.toString(array);
    }
    
}
