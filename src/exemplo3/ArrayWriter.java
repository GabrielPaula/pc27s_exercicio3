/**
 * ArrayWriter: Sincroniizacao de threads
 * Exemplo que mostra um problema na
 * escrita compartilhada de um vetor
 * por multiplas threads.
 * 
 * Autor: Gabriel Souza de Paula
 * Ultima modificacao: 01/10/2017
 */
package exemplo3;

import java.util.*;

public class ArrayWriter implements Runnable {

    private final VetorCompartilhado vetorCompartilhado;
    private int startValue; 
        
    public ArrayWriter(int value, VetorCompartilhado vetor){
        startValue=value;
        vetorCompartilhado=vetor;
    }//fim do construtor

    public void run(){
        
        for (int i=startValue; i<startValue+3; i++){
            vetorCompartilhado.add(i); //Adiciona um elemento em ordem crescente no vetor compartilhado
        }

        //Ele realizou as operações primeiro os de
        // um metodo depois os de outro, por isso no fim ficou vazio o valor
        for (int i=startValue; i<startValue + 3; i++){
            vetorCompartilhado.sub(); // Retira um elemento em ordem crescente no vetor compartilhado
        }
    }//fim run
    
}
